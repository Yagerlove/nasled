import java.util.ArrayList;

public class FinanceReportProcessor {
    public static FinanceReport getReportBySurname(FinanceReport financeReport,String surname) {
        ArrayList<Payment> paymentList = new ArrayList<>();
        for (Payment payment : financeReport.getPayments()) {
            if(payment.getName().startsWith(surname.trim())){
                paymentList.add(payment);
            }
        }
        return new FinanceReport(financeReport.getName(),financeReport.getDate(),paymentList.toArray(new Payment[paymentList.size()]));
    }
    public static final FinanceReport getReportByPayment(FinanceReport financeReport,int paymentAmount){
        ArrayList<Payment> paymentList = new ArrayList<>();
        for (Payment payment : financeReport.getPayments()) {
            if(payment.getPayment() <= paymentAmount){
                paymentList.add(payment);
            }
        }
        return new FinanceReport(financeReport.getName(),financeReport.getDate(),paymentList.toArray(new Payment[paymentList.size()]));
    }
}
