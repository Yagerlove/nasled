import static org.junit.Assert.*;
import static org.junit.Assert.assertThrows;
import org.junit.Assert;
import org.junit.Test;

public class FinanceReportTest {
    @Test
    public void TestConstructor(){
        FinanceReport financeReport  = new FinanceReport("1",1,2,3,new Payment[]{
                new Payment("abc",1,2,3,500),
                new Payment("def",2,3,4,1000)});
        Assert.assertEquals(financeReport.getName(),"1");
        Assert.assertEquals(financeReport.getDate().getDay(),1);
        Assert.assertEquals(financeReport.getDate().getEnumMonth(),Months.FEBRUARY);
        Assert.assertEquals(financeReport.getDate().getYear(),3);
    }


    @Test
    public void TestPaymentAt(){
        FinanceReport financeReport  = new FinanceReport("1",1,2,3,new Payment[]{
                new Payment("abc",1,2,3,500),
                new Payment("def",2,3,4,1000)});
        Assert.assertNotNull(assertThrows(ArrayIndexOutOfBoundsException.class,() ->{financeReport.paymentAt(4);}));
        Assert.assertEquals(financeReport.paymentAt(0).toString(),new Payment("abc",1,2,3,500).toString());
    }

    @Test
    public void TestLength(){
        FinanceReport financeReport  = new FinanceReport("1",1,2,3,new Payment[]{
                new Payment("abc",1,2,3,500),
                new Payment("def",2,3,4,1000)});
        Assert.assertEquals(financeReport.length(),2);
        Assert.assertEquals(new FinanceReport("1",1,2,3,null).length(),0);
    }
}